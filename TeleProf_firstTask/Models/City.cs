﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeleProf_firstTask.Models
{
    public class City
    {
        

        public int c_id { get; set; }
        
        public string c_name { get; set; }



        public City(int c_id, string c_name)
        {
            this.c_id = c_id;
            this.c_name = c_name;
        }

    }
}