﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeleProf_firstTask.Models;

namespace TeleProf_firstTask.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {


            List<City> cities = new List<City>(5);
            cities.Add(new City(1, "Москва"));
            cities.Add(new City(2, "Санкт-Петербург"));
            cities.Add(new City(3, "Самара"));
            cities.Add(new City(4, "Рязань"));
            cities.Add(new City(5, "Владивосток"));



            ViewBag.cities = cities;


            return View();

        }

       
    }
}